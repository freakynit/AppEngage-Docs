var express = require('express');
var router = express.Router();
var exec = require("child_process").exec;

router.handleWebhook = function(req, res){
    var rawData = req.rawBody;
	
	exec("/bin/sh build.sh", function(error, stdout, stderr) {
	    console.log('stdout: ' + stdout);
	    console.log('stderr: ' + stderr);
	    if (error !== null) {
	        console.log('exec error: ' + error);
	    }
	});
	res.status(200);
}

module.exports.router = router;
