var PORT = 11111;

var express = require('express');
var bodyParser = require('body-parser');
var url = require('url');

var app = express();

app.use(function(req, res, next) {
    if(req.url.indexOf("/webhook") > -1) {
        req.rawBody = '';
        req.on('data', function(chunk) {
            req.rawBody += chunk;
        });
        req.on('end', function() {
            require('./routes/webhook').router.handleWebhook(req, res);
        });
    } else {
        next();
    }
});

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

var router = express.Router();

app.use('/index', require('./routes/index').router);

var server = app.listen(PORT, function() {
    console.log("Listening to port %s", server.address().port);
});
